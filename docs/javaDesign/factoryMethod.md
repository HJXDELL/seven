**1、工厂方法模式（Factory Method）**

工厂方法模式分为三种：

**11、普通工厂模式**，就是建立一个工厂类，对实现了同一接口的一些类进行实例的创建。首先看下关系图：

![img](http://dl.iteye.com/upload/attachment/0083/1180/421a1a3f-6777-3bca-85d7-00fc60c1ae8b.png)

举例如下：（我们举一个发送邮件和短信的例子）

首先，创建二者的共同接口：

 

**[java]** [view plain](http://blog.csdn.net/zhangerqing/article/details/8194653)[copy](http://blog.csdn.net/zhangerqing/article/details/8194653)

1. public interface Sender {  
2. ​    public void Send();  
3. }  

 

其次，创建实现类：

 

**[java]** [view plain](http://blog.csdn.net/zhangerqing/article/details/8194653)[copy](http://blog.csdn.net/zhangerqing/article/details/8194653)

1. public class MailSender implements Sender {  
2. ​    @Override  
3. ​    public void Send() {  
4. ​        System.out.println("this is mailsender!");  
5. ​    }  
6. }  

 

**[java]** [view plain](http://blog.csdn.net/zhangerqing/article/details/8194653)[copy](http://blog.csdn.net/zhangerqing/article/details/8194653)

1. public class SmsSender implements Sender {  
2.   
3. ​    @Override  
4. ​    public void Send() {  
5. ​        System.out.println("this is sms sender!");  
6. ​    }  
7. }  

 

最后，建工厂类：

 

**[java]** [view plain](http://blog.csdn.net/zhangerqing/article/details/8194653)[copy](http://blog.csdn.net/zhangerqing/article/details/8194653)

1. public class SendFactory {  
2.   
3. ​    public Sender produce(String type) {  
4. ​        if ("mail".equals(type)) {  
5. ​            return new MailSender();  
6. ​        } else if ("sms".equals(type)) {  
7. ​            return new SmsSender();  
8. ​        } else {  
9. ​            System.out.println("请输入正确的类型!");  
10. ​            return null;  
11. ​        }  
12. ​    }  
13. }  

 

我们来测试下：

 

1. public class FactoryTest {  
2.   
3. ​    public static void main(String[] args) {  
4. ​        SendFactory factory = new SendFactory();  
5. ​        Sender sender = factory.produce("sms");  
6. ​        sender.Send();  
7. ​    }  
8. }  

 

输出：this is sms sender!

**\*22、多个工厂方法模式***，是对普通工厂方法模式的改进，在普通工厂方法模式中，如果传递的字符串出错，则不能正确创建对象，而多个工厂方法模式是提供多个工厂方法，分别创建对象。关系图：

![点击查看原始大小图片](http://dl.iteye.com/upload/attachment/0083/1181/84673ccf-ef89-3774-b5cf-6d2523cd03e5.jpg)

将上面的代码做下修改，改动下SendFactory类就行，如下：

 

**[java]** [view plain](http://blog.csdn.net/zhangerqing/article/details/8194653)[copy](http://blog.csdn.net/zhangerqing/article/details/8194653)public class SendFactory {  

   public Sender produceMail(){  

1. ​        return new MailSender();  
2. ​    }  
3. ​      
4. ​    public Sender produceSms(){  
5. ​        return new SmsSender();  
6. ​    }  
7. }  

 

测试类如下：

 

**[java]** [view plain](http://blog.csdn.net/zhangerqing/article/details/8194653)[copy](http://blog.csdn.net/zhangerqing/article/details/8194653)

1. public class FactoryTest {  
2.   
3. ​    public static void main(String[] args) {  
4. ​        SendFactory factory = new SendFactory();  
5. ​        Sender sender = factory.produceMail();  
6. ​        sender.Send();  
7. ​    }  
8. }  

 

输出：this is mailsender!

**\*33、静态工厂方法模式***，将上面的多个工厂方法模式里的方法置为静态的，不需要创建实例，直接调用即可。

 

**[java]** [view plain](http://blog.csdn.net/zhangerqing/article/details/8194653)[copy](http://blog.csdn.net/zhangerqing/article/details/8194653)

1. public class SendFactory {  
2. ​      
3. ​    public static Sender produceMail(){  
4. ​        return new MailSender();  
5. ​    }  
6. ​      
7. ​    public static Sender produceSms(){  
8. ​        return new SmsSender();  
9. ​    }  
10. }  

 

**[java]** [view plain](http://blog.csdn.net/zhangerqing/article/details/8194653)[copy](http://blog.csdn.net/zhangerqing/article/details/8194653)

1. public class FactoryTest {  
2.   
3. ​    public static void main(String[] args) {      
4. ​        Sender sender = SendFactory.produceMail();  
5. ​        sender.Send();  
6. ​    }  
7. }  

 

输出：this is mailsender!

总体来说，工厂模式适合：凡是出现了大量的产品需要创建，并且具有共同的接口时，可以通过工厂方法模式进行创建。在以上的三种模式中，第一种如果传入的字符串有误，不能正确创建对象，第三种相对于第二种，不需要实例化工厂类，所以，大多数情况下，我们会选用第三种——静态工厂方法模式。

**2、抽象工厂模式（Abstract Factory）**

工厂方法模式有一个问题就是，类的创建依赖工厂类，也就是说，如果想要拓展程序，必须对工厂类进行修改，这违背了闭包原则，所以，从设计角度考虑，有一定的问题，如何解决？就用到抽象工厂模式，创建多个工厂类，这样一旦需要增加新的功能，直接增加新的工厂类就可以了，不需要修改之前的代码。因为抽象工厂不太好理解，我们先看看图，然后就和代码，就比较容易理解。

![点击查看原始大小图片](http://dl.iteye.com/upload/attachment/0083/1185/34a0f8de-16e0-3cd5-9f69-257fcb2be742.jpg)

请看例子：

 

**[java]** [view plain](http://blog.csdn.net/zhangerqing/article/details/8194653)[copy](http://blog.csdn.net/zhangerqing/article/details/8194653)

1. public interface Sender {  
2. ​    public void Send();  
3. }  

 

两个实现类：

 

**[java]** [view plain](http://blog.csdn.net/zhangerqing/article/details/8194653)[copy](http://blog.csdn.net/zhangerqing/article/details/8194653)

1. public class MailSender implements Sender {  
2. ​    @Override  
3. ​    public void Send() {  
4. ​        System.out.println("this is mailsender!");  
5. ​    }  
6. }  

 

**[java]** [view plain](http://blog.csdn.net/zhangerqing/article/details/8194653)[copy](http://blog.csdn.net/zhangerqing/article/details/8194653)

1. public class SmsSender implements Sender {  
2.   
3. ​    @Override  
4. ​    public void Send() {  
5. ​        System.out.println("this is sms sender!");  
6. ​    }  
7. }  

 

两个工厂类：

 

**[java]** [view plain](http://blog.csdn.net/zhangerqing/article/details/8194653)[copy](http://blog.csdn.net/zhangerqing/article/details/8194653)

1. public class SendMailFactory implements Provider {  
2. ​      
3. ​    @Override  
4. ​    public Sender produce(){  
5. ​        return new MailSender();  
6. ​    }  
7. }  

 

**[java]** [view plain](http://blog.csdn.net/zhangerqing/article/details/8194653)[copy](http://blog.csdn.net/zhangerqing/article/details/8194653)

1. public class SendSmsFactory implements Provider{  
2.   
3. ​    @Override  
4. ​    public Sender produce() {  
5. ​        return new SmsSender();  
6. ​    }  
7. }  

 

在提供一个接口：

 

**[java]** [view plain](http://blog.csdn.net/zhangerqing/article/details/8194653)[copy](http://blog.csdn.net/zhangerqing/article/details/8194653)

1. public interface Provider {  
2. ​    public Sender produce();  
3. }  

 

测试类：

 

**[java]** [view plain](http://blog.csdn.net/zhangerqing/article/details/8194653)[copy](http://blog.csdn.net/zhangerqing/article/details/8194653)

1. public class Test {  
2.   
3. ​    public static void main(String[] args) {  
4. ​        Provider provider = new SendMailFactory();  
5. ​        Sender sender = provider.produce();  
6. ​        sender.Send();  
7. ​    }  
8. }  

 

其实这个模式的好处就是，如果你现在想增加一个功能：发及时信息，则只需做一个实现类，实现Sender接口，同时做一个工厂类，实现Provider接口，就OK了，无需去改动现成的代码。这样做，拓展性较好！