#### 基本语法

​	1.注解：单行注解：#      多行注解：’‘’，”“”

​	2.缩进标识代码块

​	3.多行语句链接,,反斜杠：\

​	4.数字类型：int,bool,float,complex(复数) 1+2j  1.1+2.2j

​	5.字符串：单引号，双引号效果一样

​	6.命令行参数：python -h

​	7.标准数据类型：Number，String ，List(列表)，Tuple(元组)，Set(集合)，Dictionary(字典)

​	8.List(列表) 变量[头下标：尾下标] 0开始，-1最后一个，+运算符，*重复操作

​	9.Touple(元组)，元组的元素不能修改！

​		tup1 = ()    # 空元组

​		tup2 = (20,) # 一个元素，需要在元素后添加逗号

​	10.Set(集合)：无序不重复元素，{}/set()表示

​	    注意：创建一个空集合必须用 set() 而不是 { }，因为 { } 是用来创建一个空字典。 

​	11.Dictionary（字典）

​		列表是有序的对象集合，**字典**是无序的对象集合。两者之间的区别在于：字典当中的元素是通过键来存取的，而不是通过偏移存取。 

​		字典是一种映射类型，字典用"{ }"标识，它是一个无序的**键(key) : 值(value)**对集合。

键(key)必须使用不可变类型。

```

```

=======0625

#### 迭代器与生成器	

**iter()** 和 **next()**

```
#!/usr/bin/python3
 
list=[1,2,3,4]
it = iter(list)    # 创建迭代器对象
for x in it:
    print (x, end=" ")
```

